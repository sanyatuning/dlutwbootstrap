<?php

namespace DluTwBootstrap;

/**
 *
 * @author sanyatuning
 */
class Alert implements AlertInterface {

    protected $message = '';
    protected $prefix = '';
    protected $close = true;
    protected $type = self::TYPE_ALERT;

    public function __construct($message, $type = self::TYPE_ALERT, $prefix = '', $close = true) {
        $this->message = $message;
        $this->setType($type);
        $this->prefix = $prefix;
        $this->close = $close;
    }

    public function setType($type) {
        $types = array(
            self::TYPE_ALERT,
            self::TYPE_ERROR,
            self::TYPE_INFO,
            self::TYPE_SUCCESS,
        );
        if (in_array($type, $types)) {
            $this->type = $type;
            return $this;
        }
        throw new \Exception('Incorrect Alert type');
    }

    public function getType() {
        return $this->type;
    }

    public function getPrefix() {
        return $this->prefix;
    }

    public function getMessage() {
        return $this->message;
    }

    public function hasCloseButton() {
        return $this->close == true;
    }

    public function __toString() {
        return $this->getMessage();
    }

}
