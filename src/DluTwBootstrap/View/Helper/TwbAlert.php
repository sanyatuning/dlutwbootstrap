<?php

namespace DluTwBootstrap\View\Helper;

use Zend\View\Helper\AbstractHelper;
use DluTwBootstrap\AlertInterface;
/**
 * TwbTabs
 * @package DluTwBootstrap
 * @copyright David Lukas (c) - http://www.zfdaily.com
 * @license http://www.zfdaily.com/code/license New BSD License
 * @link http://www.zfdaily.com
 * @link https://bitbucket.org/dlu/dlutwbootstrap
 */
class TwbAlert extends AbstractHelper {


    /* ********************** METHODS *************************** */

    /**
     * Renders helper
     * 
     * @param string|\DluTwBootstrap\AlertInterface $message
     * @param string $type
     * @param string $strong
     * @param boolean $close
     * @return string HTML code
     */
    public function render($message, $type = AlertInterface::TYPE_ALERT, $strong = '', $close = true) {
        if ($message instanceof AlertInterface) {
            $type = $message->getType();
            $close = $message->hasCloseButton();
            $strong = $message->getPrefix();
            $message = $message->getMessage();
        }
        $button = $close ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '';
        $strong = strlen($strong) ? sprintf('<strong>%s</strong> ', $strong) : '';
        $html = sprintf('<div class="%s">%s%s%s</div>', $type, $button, $strong, $message);
        return $html;
    }

    public function __invoke($message, $type = AlertInterface::TYPE_ALERT, $strong = '', $close = true) {
        return $this->render($message, $type, $strong, $close);
    }

}