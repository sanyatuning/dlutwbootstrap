<?php

namespace DluTwBootstrap;

/**
 *
 * @author sanyatuning
 */
interface AlertInterface {

    const TYPE_ALERT   = 'alert';
    const TYPE_ERROR   = 'alert alert-error';
    const TYPE_INFO    = 'alert alert-info';
    const TYPE_SUCCESS = 'alert alert-success';

    public function getType();
    
    public function getPrefix();
    
    public function getMessage();
    
    public function hasCloseButton();
    
    public function __toString();
    
}
