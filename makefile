all: css

# target: help - Display callable targets.
help:
	egrep "^# target:" [Mm]akefile

# target: compass - compile SASS/SCSS files with compass
css:
	compass compile --boring --sass-dir src/css/ --css-dir public/css/

